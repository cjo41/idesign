package uk.ac.cam.idesign.group11;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;

import java.util.HashMap;
import java.util.Map;

/**
 * Inner container pane for weather data, sits within Container.
 * @author Group11
 */
public class DataPane extends GridPane {
    
    private Map<WeatherData.WeatherType, Image> images;
    private Map<WeatherData.WeatherType, String> descriptions;
    private ImageView mWeather, mWind;
    private Text mWeatherDescription, mWindDescription;
    
    public DataPane(WeatherData w) {
        // Configuring pane properties.
        this.setPadding(new Insets(0, 0, 15, 0));
        ColumnConstraints col1 = new ColumnConstraints();
        ColumnConstraints col2 = new ColumnConstraints();
        col1.setPercentWidth(50);
        col1.setHalignment(HPos.CENTER);
        col2.setPercentWidth(50);
        col2.setHalignment(HPos.CENTER);
        this.getColumnConstraints().addAll(col1, col2);
    
        // Populating data.
        images = new HashMap<>();
        descriptions = new HashMap<>();
        populateMaps();
    
        // Creating nodes.
        mWeather = createWeather(w);
        mWind = createWindArrow(w);
        mWeatherDescription = createWeatherDescriptionText(w);
        mWindDescription = createWindDescriptionText(w);
    
        // Adding nodes to pane.
        this.add(mWeather, 0, 0);
        this.add(mWind, 1, 0);
        this.add(mWeatherDescription, 0, 1);
        this.add(mWindDescription, 1, 1);
    }
    
    /**
     * Removes nodes currently displayed in pane, creates new nodes
     * corresponding to the WeatherData object passed as argument, and shows
     * these. I.e. updates the weather details shown.
     * @param w     Weather conditions to display.
     */
    public void update(WeatherData w) {
        this.getChildren().remove(mWeather);
        this.getChildren().remove(mWind);
        this.getChildren().remove(mWeatherDescription);
        this.getChildren().remove(mWindDescription);
        
        mWeather = createWeather(w);
        mWind = createWindArrow(w);
        mWeatherDescription = createWeatherDescriptionText(w);
        mWindDescription = createWindDescriptionText(w);
    
        this.add(mWeather, 0, 0);
        this.add(mWind, 1, 0);
        this.add(mWeatherDescription, 0, 1);
        this.add(mWindDescription, 1, 1);
    }
    
    /**
     * Instantiates ImageView object for wind arrow.
     * @return      Wind arrow graphic.
     */
    private ImageView createWindArrow(WeatherData w) {
        Image arrow = new Image("media/windarrow.png");
        ImageView windView = new ImageView();
        windView.setImage(arrow);
        windView.setFitWidth(130);
        windView.setPreserveRatio(true);
        windView.setRotate(30);
        return windView;
    }
    
    /**
     * Instantiates ImageView object for current weather conditions.
     * @param w     Current weather.
     * @return      Weather graphic.
     */
    private ImageView createWeather(WeatherData w) {
        ImageView wView = new ImageView();
        wView.setImage(images.get(w.getType()));
        wView.setFitWidth(130);
        wView.setPreserveRatio(true);
        return wView;
    }
    
    /**
     * Instantiates Text object which describes current
     * weather conditions.
     * @param w     Current weather.
     * @return      Description for that weather type.
     */
    private Text createWeatherDescriptionText(WeatherData w) {
        Text title = new Text(descriptions.get(w.getType()) + "\n" + (int) w.getTemp() + "°C");
        title.setFont(Font.font("Segoe UI", FontWeight.BOLD, 25));
        title.setFill(Color.valueOf("#156AA3"));
        title.setTextAlignment(TextAlignment.CENTER);
        return title;
    }
    
    /**
     * Instantiates Text object which describes current
     * wind speed.
     * @param w     Current weather.
     * @return      Description for that wind speed.
     */
    private Text createWindDescriptionText(WeatherData w) {
        Text title = new Text((int) w.getWindSpeed() + " mph\n");
        title.setFont(Font.font("Segoe UI", FontWeight.BOLD, 25));
        title.setFill(Color.valueOf("#156AA3"));
        title.setTextAlignment(TextAlignment.CENTER);
        return title;
    }
    
    /**
     * Populates `images` and `descriptions` HashMaps.
     */
    private void populateMaps() {
        images.put(WeatherData.WeatherType.CLOUD, new Image("media/cloud.png"));
        descriptions.put(WeatherData.WeatherType.CLOUD, "Cloudy");
        images.put(WeatherData.WeatherType.SUN, new Image("media/sun.png"));
        descriptions.put(WeatherData.WeatherType.SUN, "Clear");
        images.put(WeatherData.WeatherType.SUN_CLOUD, new Image("media/sun-cloud.png"));
        descriptions.put(WeatherData.WeatherType.SUN_CLOUD, "Light cloud");
        images.put(WeatherData.WeatherType.HEAVY_RAIN, new Image("media/rain-heavy.png"));
        descriptions.put(WeatherData.WeatherType.HEAVY_RAIN, "Heavy rain");
        images.put(WeatherData.WeatherType.LIGHT_RAIN, new Image("media/rain-light.png"));
        descriptions.put(WeatherData.WeatherType.LIGHT_RAIN, "Light rain");
        images.put(WeatherData.WeatherType.SNOW, new Image("media/snow.png"));
        descriptions.put(WeatherData.WeatherType.SNOW, "Snow");
    }
}
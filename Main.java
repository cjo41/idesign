package uk.ac.cam.idesign.group11;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Main execution class of application.
 * @author cjo41
 */
public class Main extends Application {
    
    private static List<WeatherData> forecasts = new ArrayList<>();
    private static final String forecastURL = "http://api.openweathermap.org/data/" +
            "2.5/forecast?q=cambridge%2Cuk&appid=33e45df825954deb5071f2ec2395614b";
    private static final String currentURL = "http://api.openweathermap.org/data/" +
            "2.5/weather?q=cambridge%2Cuk&appid=33e45df825954deb5071f2ec2395614b";
    private AlarmClock mClock = new AlarmClock();
    private Scene mHomeScene = initHomeScene();
    private Scene mAlarmScene = initAlarmScene();
    private Stage mPrimaryStage;
    private boolean mSafe;
    
    /**
     * Returns a Weather object containing the forecast for the
     * given timescale.
     * @param timescale     Timescale, from 0 to 3.
     * @return              Weather object.
     */
    public static WeatherData getForecast(int timescale) {
        return forecasts.get(timescale);
    }
    
    /**
     * Gets forecast data, constructs Weather objects and
     * saves in List forecasts.
     */
    public static void refreshForecasts() {
        forecasts.add(new WeatherData(currentURL));
        for (int i=0; i<3; i++) {
            forecasts.add(new WeatherData(forecastURL, i));
        }
    }
    
    
    /**
     * Initializes the home screen and returns a pointer to it.
     * @return  Scene object.
     */
    private Scene initHomeScene() {
        refreshForecasts();
        int safetyScore = ScorePane.calcScore(forecasts.get(0));
        Flag.FlagType flagColour = Flag.getFlag();
        boolean safe = (flagColour == Flag.FlagType.GREEN);
        mSafe=safe;
        System.out.println(safetyScore);
        // Instantiating nodes.
        BorderPane homeContainer = new Container();
        GridPane homeCentre = new GridPane();
        
        GridPane homeHeader = new HeaderPane();
        GridPane homeFlag = new FlagPane(safe);
        GridPane homeScore = new ScorePane(forecasts.get(0), safe);
        GridPane homeText = new TextPane();
        DataPane homeData = new DataPane(forecasts.get(0));
        GridPane homeBottom = new BottomPane(homeData);
        
        // Adding to container.
        homeContainer.setTop(homeHeader);
        homeContainer.setCenter(homeCentre);
        homeCentre.add(homeFlag, 0, 0);
        homeCentre.add(homeScore, 0, 1);
        homeCentre.add(homeText, 0, 2);
        homeCentre.add(homeData, 0, 3);
        homeContainer.setBottom(homeBottom);
        
        // Adding container to scene.
        Scene homeScene = new Scene(homeContainer, 360, 640);
        // Creating alarm icon.
        Image alarm = new Image("media/alarm.png");
        ImageView i = new ImageView();
        i.setImage(alarm);
        i.setFitHeight(45);
        i.setPreserveRatio(true);
        // EventHandler for alarm icon being clicked.
        i.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                System.out.println("Alarm Button Clicked");
                changeScene(mPrimaryStage,mHomeScene,mAlarmScene,2);
                
            }
        });
        homeHeader.add(i,1,0);
        return homeScene;
    }
    
    /**
     * Initializes the alarm scene.
     * @return      Returns a Scene object.
     */
    private Scene initAlarmScene() {
        // Instantiating nodes.
        BorderPane alarmContainer = new Container();
        
        GridPane alarmCentre = new GridPane();
        GridPane alarmHeader = new GoingBackPane();
        GridPane alarmWeekButtons = new DaysOfTheWeekPane(mClock);
        GridPane alarmPanel = new AlarmPane(mClock);
        
        alarmContainer.setTop(alarmHeader);
        alarmContainer.setCenter(alarmCentre);
        alarmCentre.add(alarmWeekButtons, 0, 1);
        alarmCentre.add(alarmPanel, 0, 2);
        
        // Adding container to scene.
        Scene alarmScene = new Scene(alarmContainer, 360, 640);
        Image alarmArrow = new Image("media/left-arrow.png");
        ImageView alarmArrowImage = new ImageView();
        alarmArrowImage.setImage(alarmArrow);
        alarmArrowImage.setFitHeight(45);
        alarmArrowImage.setPreserveRatio(true);
        
        // EventHandler for arrow icon being clicked.
        alarmArrowImage.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                System.out.println("Go back to main menu!");
                changeScene(mPrimaryStage,mHomeScene,mAlarmScene,1);
            }
        });
        alarmHeader.add(alarmArrowImage, 1, 0);
        return alarmScene;
    }
    /**
     * Displays the main screen, with the flag, safe/unsafe criterion,
     * weather data and all other pertinent information.
     * Also runs a recurring check on the alarm clock.
     * @param primaryStage  Stage object.
     * @throws Exception    Inherited from parent.
     */
    private void showHomeScreen(Stage primaryStage) throws Exception {
        mPrimaryStage = primaryStage;
        primaryStage.setScene(mHomeScene);
        primaryStage.setTitle("Weather Application");
        primaryStage.show();
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                mClock.ringCheck(!mSafe);
            }
        };
        Timer timer = new Timer();
        timer.schedule(task,0,1000);
    }
    
    /**
     * Facilitates the change between scenes for the primary stage.
     * @param primaryStage  Has the stage as an input.
     * @param s1            Home Scene.
     * @param s2            Alarm Scene.
     * @param number        Parameter - 1 to switch to home scene, otherwise switch to alarm scene.
     * */
    private void changeScene(Stage primaryStage, Scene s1, Scene s2, int number){
        if (number == 1) {
            primaryStage.setScene(s1);
            primaryStage.setTitle("Weather Application");
        }
        else {
            primaryStage.setScene(s2);
            primaryStage.setTitle("Alarm Clock");
        }
        primaryStage.show();
    }
    
    public static void main(String[] args) {
        launch(args);
    }
    
    @Override
    public void start(Stage primaryStage) throws Exception {
        showHomeScreen(primaryStage);
    }
}
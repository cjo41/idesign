package uk.ac.cam.idesign.group11;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

/**
 * Shows a "Here's the weather" welcome message.
 * @author Group11
 */
public class TextPane extends GridPane {
    
    public TextPane() {
        this.setAlignment(Pos.CENTER);
        this.setPadding(new Insets(25, 0, 25, 0));
        
        // Creating title element.
        Text title = new Text("Here's the weather");
        title.setFont(Font.font("Segoe UI", FontWeight.BOLD, 20));
        title.setFill(Color.valueOf("#156AA3"));
        this.add(title, 0, 0);
    }
}
package uk.ac.cam.idesign.group11;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;

import java.util.ArrayList;
import java.util.List;

/**
 * Pane to show days of the week buttons on alarm screen.
 * @author Group11
 */
public class DaysOfTheWeekPane extends GridPane {
    
    private List<DaysOfTheWeekButton> buttons = new ArrayList<>();
    
    public DaysOfTheWeekPane(AlarmClock clock){
        // Configuring pane properties.
        this.setPadding(new Insets(25, 0, 15, 0));
        this.setMinWidth(360);
        this.setMaxHeight(105);
        this.setAlignment(Pos.CENTER);
        this.setStyle("-fx-background-color: #BDE0F7");
        
        // Defining column width constraints.
        ColumnConstraints col1 = new ColumnConstraints();
        this.getColumnConstraints().addAll(col1, col1, col1, col1, col1, col1, col1);
        
        // Creates four buttons for the days of the week
        for (int i=0; i < 7; i++) {
            DaysOfTheWeekButton clkB = new DaysOfTheWeekButton(i, this,clock);
            clkB.setPrefSize(51, 51);
            buttons.add(clkB);
            this.add(buttons.get(i), i, 0);
        }
        refreshHighlighting();
    }
    
    /**
     * Refreshes button highlighting based on whether selected or not.
     */
    public void refreshHighlighting() {
        for (DaysOfTheWeekButton b : buttons) {
            if (b.selected) b.setStyle("-fx-background-color: #BDE0F7");
            else b.setStyle("-fx-background-color: #7FC2EF");
        }
    }
    
    /**
     * Sets all the buttons to deselected.
     */
    public void setAllDeselected() {
        for (DaysOfTheWeekButton b : buttons) {
            b.selected = false;
        }
    }
}
package uk.ac.cam.idesign.group11;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;

import java.util.ArrayList;
import java.util.List;

/**
 * Pane to allow selection of different forecast times.
 * Extends JavaFX's GridPane.
 * @author Group11
 */
public class BottomPane extends GridPane {
    
    private List<TimeButton> buttons = new ArrayList<>();
    
    public BottomPane(DataPane dataPane) {
        // Configuring pane properties.
        this.setAlignment(Pos.CENTER);
        this.setPadding(new Insets(15, 0, 0, 0));
        this.setMaxWidth(360);
        
        // Defining column width constraints.
        ColumnConstraints col1 = new ColumnConstraints();
        col1.setPercentWidth(25);
        this.getColumnConstraints().addAll(col1, col1, col1, col1);
        
        // Creates four buttons at bottom of screen.
        for (int i=0; i<4; i++) {
            buttons.add(new TimeButton(i, this, dataPane));
            this.add(buttons.get(i), i, 0);
        }
        buttons.get(0).selected = true;
        refreshHighlighting();
    }
    
    /**
     * Iterates through buttons and sets highlighting correctly.
     */
    public void refreshHighlighting() {
        for (TimeButton b : buttons) {
            if (b.selected) b.setStyle("-fx-background-color: #BDE0F7");
            else b.setStyle("-fx-background-color: #7FC2EF");
        }
    }
    
    /**
     * Sets all of the bottom buttons to deselected.
     */
    public void setAllDeselected() {
        for (TimeButton b : buttons) {
            b.selected = false;
        }
    }
}
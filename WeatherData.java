package uk.ac.cam.idesign.group11;

import java.io.*;
import java.net.URL;
import java.nio.charset.Charset;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * This will read a JSON file and extract relevant data: General
 * weather (cloudy, sunny, rainy, snowy, sun+clouds, light rain), #
 * temperature and wind speed.
 * @author Group11
 */
public class WeatherData {
    
    public static enum WeatherType {
        LIGHT_RAIN,
        HEAVY_RAIN,
        SUN,
        SUN_CLOUD,
        CLOUD,
        SNOW;
    }
    
    private WeatherType mType = WeatherType.SUN;
    private double mTemp;
    private double mWindSpeed;
    
    /**
     * Reads all text from initialised Reader object.
     * @param rd                Initialised Reader.
     * @return                  String of text pointed to by Reader.
     * @throws IOException     If file reading fails.
     */
    private static String readAll(Reader rd) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }
    
    /**
     * Creates a JSONObject from the url given as parameter.
     * @param url               URL of weather forecast data.
     * @return                  Object created from the data at that URL.
     * @throws IOException     If file reading fails.
     * @throws JSONException   If can't parse JSON successfully.
     */
    private static JSONObject readJsonFromUrl(String url) throws IOException, JSONException {
        try (InputStream is = new URL(url).openStream()) {
            BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
            String jsonText = readAll(rd);
            return new JSONObject(jsonText);
        }
    }
    
    public WeatherData(String requestURL, int index) {
        try {
            // Creating JSONObject.
            JSONObject forecast = readJsonFromUrl(requestURL);
            JSONArray weatherList = forecast.getJSONArray("list");
            JSONObject wantedWeather = weatherList.getJSONObject(index);
            
            // Getting temperature.
            mTemp = wantedWeather.getJSONObject("main").getDouble("temp");
            mTemp -= 273.15;
            mTemp = Math.round(mTemp);
    
            // Getting weather type.
            // For some reason, this is held as a JSON array but it always has only one element.
            String type = wantedWeather.getJSONArray("weather")
                                        .getJSONObject(0).getString("main");
            setType(type);
            
            // Getting wind speed.
            mWindSpeed = wantedWeather.getJSONObject("wind").getDouble("speed");
            mWindSpeed = Math.round(mWindSpeed);
        }
        catch (IOException | JSONException e) {
            System.out.println(e.getMessage());
        }
    }
    
    public WeatherData (String requestURL) {
        try {
            // Creating JSONObject.
            JSONObject JSONWeather = readJsonFromUrl(requestURL);
            JSONArray Aux = JSONWeather.getJSONArray("weather");
            
            // Getting weather type.
            // For some reason, this is held as a JSON array but it always has only one element
            String type = Aux.getJSONObject(0).getString("main");
            setType(type);
            
            // Getting temperature.
            mTemp = JSONWeather.getJSONObject("main").getDouble("temp");
            mTemp -= 273.15;
            mTemp = Math.round(mTemp);
            
            // Getting wind speed.
            mWindSpeed = JSONWeather.getJSONObject("wind").getDouble("speed");
            mWindSpeed = Math.round(mWindSpeed);
        }
        catch (IOException | JSONException e) {
            System.out.println(e.getMessage());
        }
    }
    
    /**
     * Takes the type as parsed in the JSONObject and updates the mType
     * variable to the correct enum type.
     * @param type  String from JSONObject containing type.
     */
    private void setType(String type) {
        if (type.equals("Clouds")) mType = WeatherType.CLOUD;
        if (type.equals("Rain")) mType = WeatherType.HEAVY_RAIN;
        if (type.equals("Mist")) mType = WeatherType.LIGHT_RAIN;
        if (type.equals("Clear")) mType = WeatherType.SUN;
        if (type.equals("broken clouds")) mType = WeatherType.SUN_CLOUD;
        if (type.equals("Snow")) mType = WeatherType.SNOW;
    }
    
    /**
     * Gets the weather type stored in this object.
     * @return  Weather type, as enum.
     */
    public WeatherType getType(){
        return mType;
    }
    
    /**
     * Gets the wind speed stored in this object.
     * @return  Wind speed.
     */
    public double getWindSpeed(){
        return mWindSpeed;
    }
    
    /**
     * Gets the temperature stored in this object.
     * @return  Temperature.
     */
    public double getTemp() {
        return mTemp;
    }
}
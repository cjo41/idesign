package uk.ac.cam.idesign.group11;

import java.io.FileInputStream;
import java.time.Clock;
import java.time.ZoneId;
import javazoom.jl.player.Player;

/**
 * Representation of alarm clock - includes methods to get, set and
 * run alarms.
 * @author Group11
 */
public class AlarmClock {
    
    // Time for a day in miliseconds, will be used for the whole alarm system.
    private static final long MILIS_DAY = 86400000;
    // Time the alarm is supposed to ring for rowing/def alarms.
    private long mRowAlarm, mDefAlarm;
    private Clock mClock = Clock.system(ZoneId.systemDefault());
    // Bool values for whether it has to ring today for rowing time and/or for def time
    private boolean mRowToday,mDefToday;
    // Array that stores whether the alarm for rowing rings on a certain day, where day 0=Monday, day 6=Sunday.
    private boolean[] mRowDays=new boolean[8];
    // Same as above, but for default ringing.
    private boolean[] mDefDays=new boolean[8];
    // Previous day, used to make certain updates at midnight
    private int mPrevDay=-1;
    // Filepath to the alarm mp3 file as a string
    private String mAlarmPath="Alarm.mp3";
    
    /**
     * Gets the clock object.
     * @return  Clock.
     */
    public Clock getClock() {
        return mClock;
    }
    
    /**
     * Sets the alarm path.
     * @param s Path of alarm as string.
     */
    public void setAlarmPath(String s) {
        mAlarmPath=s;
    }
    
    /**
     * Gets the rowing alarm time.
     * @return  Time set for rowing alarm.
     */
    public long getRowAlarm() {
        return mRowAlarm;
    }
    
    /**
     * Sets the rowing alarm time based on hours and minutes.
     * @param hour      Hour at which alarm is to be set.
     * @param minute    Minute at which alarm is to be set.
     */
    public void setRowAlarm(int hour,int minute) {
        // The time is expressed in miliseconds passed since 1 Jan 1970.
        mRowAlarm = (mClock.millis() - (mClock.millis() % MILIS_DAY)) +
                (hour-1)*3600000 + (minute*60000);
        // In case the alarm time already passed today, we don't want the app to randomly
        // just ring when the user sets the time
        if (mRowAlarm < mClock.millis()) mRowAlarm += MILIS_DAY;
    }
    
    /**
     * Gets the default alarm time.
     * @return  Time set for default alarm.
     */
    public long getDefAlarm() {
        return mDefAlarm;
    }
    
    /**
     * Sets the default alarm time based on hours and minutes.
     * @param hour      Hour at which alarm is to be set.
     * @param minute    Minute at which alarm is to be set.
     */
    public void setDefAlarm(int hour,int minute) {
        // The time is expressed in miliseconds passed since 1 Jan 1970.
        mDefAlarm = (mClock.millis()-mClock.millis()% MILIS_DAY)+(hour-1)*3600000+minute*60000+10000;
        // In case the alarm time already passed today, we don't want the app to randomly
        // just ring when the user sets the time
        if (mDefAlarm<mClock.millis()) mDefAlarm+= MILIS_DAY;
    }
    
    /**
     * Sets whether rowing alarm is to sound on given day.
     * @param x     True if alarm is to sound.
     * @param day   Day index, from 0 to 6.
     */
    public void setRow(boolean x,int day) {
        mRowDays[day]=x;
    }
    
    /**
     * Sets whether default alarm is to sound on given day.
     * @param x     True if alarm is to sound.
     * @param day   Day index, from 0 to 6.
     */
    public void setDef(boolean x,int day) {
        mDefDays[day]=x;
    }
    
    /**
     * Returns true if rowing alarm is to sound today.
     * @return  Whether rowing alarm sounds.
     */
    public boolean getRowToday() {
        return mRowToday;
    }
    
    /**
     * Returns true if default alarm is to sound today.
     * @return  Whether default alarm sounds.
     */
    public boolean getDefToday() {
        return mDefToday;
    }
    
    /**
     * Calculates current day of the week.
     * @return  Day index, from 0 to 6.
     */
    public int getDay() {
        long x = (mClock.millis()/ MILIS_DAY +3)%7;
        return (int)x;
    }
    
    /**
     * Rings if alarm time has been reached.
     * @param param     For debugging - indicates whether rowing or default alarm.
     * @param filepath  Alarm ringtone.
     */
    public void ring(String param, String filepath) {
        System.out.println("Time to wake up!!!!!"+param);
        System.out.println(mClock.instant());
        try {
            FileInputStream fIS=new FileInputStream(filepath);
            Player player=new Player(fIS);
            player.play();
            System.out.println(1);
        }
        catch (Exception e) {
            System.out.println("oops");
            e.printStackTrace();
        }
    }
    
    /**
     * Checks whether alarm time has been reached.
     * @param flag  True if yellow/red flag is showing.
     */
    public void ringCheck(boolean flag) {
        int day=this.getDay();
        
        // Update today's data for rowing.
        if (day!=mPrevDay) {
            mRowToday = mRowDays[day];
            mDefToday = mDefDays[day];
        }
        
        // For rowing alarm.
        if (mRowToday) {
            if (mClock.millis() > mRowAlarm) {
                // If flag showing, don't ring.
                if (flag) {
                    mRowToday=false;
                    mDefToday=true;
                }
                else {
                    this.ring("Row!",mAlarmPath);
                }
            }
        }
        
        // For default alarm.
        if (mDefToday) {
            if(mClock.millis() > mDefAlarm) {
                ring("Default!",mAlarmPath);
            }
        }
    
        // If the time of the alarm passed for today, update to tomorrow's time.
        if (mClock.millis() > mRowAlarm) mRowAlarm += MILIS_DAY;
        if (mClock.millis() > mDefAlarm) mDefAlarm += MILIS_DAY;
    }
}
package uk.ac.cam.idesign.group11;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;

/**
 * Manages representation and retrieval of the safety flag,
 * as provided by the CUBC website.
 * @author Group11
 */
public class Flag {
    
    public enum FlagType {
        GREEN, YELLOW, RED;
    }
    
    /**
     * Gets the current flag status from the CUBC website.
     * @return  Current flag status.
     */
    public static FlagType getFlag() {
        FlagType colour = FlagType.RED;
        try {
            URL url = new URL("http://www.cucbc.org/flag.txt");
            BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
            String flag;
            flag = in.readLine();
            if (flag.startsWith("g")) colour = FlagType.GREEN;
            if (flag.startsWith("y")) colour = FlagType.YELLOW;
            if (flag.startsWith("r")) colour = FlagType.RED;
        } catch (Exception e) {
            System.out.println("Couldn't retrieve URL.");
            e.printStackTrace();
        }
        return colour;
    }
}
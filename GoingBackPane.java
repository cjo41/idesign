package uk.ac.cam.idesign.group11;

import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

public class GoingBackPane extends GridPane {

    /**
     *Constructor Function
     */
    public GoingBackPane() {
        // Configuring pane properties.
        this.setAlignment(Pos.TOP_LEFT);
        this.setPadding(new Insets(15, 0, 15, 15));


        // Creating title element.
        Text title = new Text("");
        title.setFont(Font.font("Roboto", FontWeight.BOLD, 20));
        title.setFill(Color.BLACK);
        this.add(title, 0, 0);
    }
}

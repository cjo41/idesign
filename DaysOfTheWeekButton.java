package uk.ac.cam.idesign.group11;


import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;

public class DaysOfTheWeekButton extends GridPane {

    public boolean selected;
    private DaysOfTheWeekPane mParent;

    /**
     * Constructor for a Day Button
     * @param timescale     Takes the day as an int value (Monday=0, Sunday=6)
     * @param parent        Takes the parent pane
     * @param clock         Takes a reference to the  AlarmClock to operate on it
     */
    public DaysOfTheWeekButton(int timescale, DaysOfTheWeekPane parent,AlarmClock clock) {
        // Configuring pane properties.
        mParent = parent;
        this.setAlignment(Pos.CENTER);
        this.setMaxSize(51, 50);

        // Defining behaviour when button is clicked.
        this.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                // Updating button highlighting.
                if(selected==false) {
                    selected = true;
                    clock.setRow(true, timescale);
                }
                else {
                    selected=false;
                    clock.setRow(false, timescale);
                }
                mParent.refreshHighlighting();
            }
        });
        this.add(createText(timescale), 0, 0);
    }

    /**
     * Text initializer
     * @param timescale     Takes day of the week as an integer value (Monday=0, Sunday=6)
     * @return
     */
    private Text createText(int timescale) {
        String text;
        if (timescale == 0) text = "Monday";
        else if (timescale == 1) text = "Tuesday";
        else if (timescale == 2) text = "Wednesday";
        else if (timescale == 3) text = "Thursday";
        else if (timescale == 4) text = "Friday";
        else if (timescale == 5) text = "Saturday";
        else if (timescale == 6) text = "Sunday";
        else throw new RuntimeException();

        Text toDisplay = new Text(text);
        toDisplay.setFont(Font.font("Roboto", FontWeight.BOLD, 11));
        toDisplay.setFill(Color.BLACK);
        toDisplay.setTextAlignment(TextAlignment.CENTER);
        return toDisplay;
    }
}
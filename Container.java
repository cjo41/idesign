package uk.ac.cam.idesign.group11;

import javafx.scene.image.Image;
import javafx.scene.layout.*;

/**
 * Outer container pane for application. Extends JavaFX's GridPane.
 * @author Group11
 */
public class Container extends BorderPane {

    public Container() {
        setBackground();
    }
    
    /**
     * Sets screen's main background image.
     */
    private void setBackground() {
        Image bg = new Image("media/bg.png");
        this.setBackground(new Background(new BackgroundImage(bg, BackgroundRepeat.NO_REPEAT,
                BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER,
                new BackgroundSize(360, 640, false, false, false, false))));
    }
}
package uk.ac.cam.idesign.group11;

import javafx.geometry.Pos;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;

/**
 * Pane to show safety score out of five using horizontal bar.
 * Extends JavaFX's GridPane.
 * @author Group11
 */
public class ScorePane extends GridPane {
    
    public ScorePane(WeatherData w, boolean safe) {
        // Configuring pane.
        this.setAlignment(Pos.CENTER);
        setBackground(safe);
        this.setMinHeight(74);
        int score = calcScore(w);
        
        // Showing image of bar indicating score.
        Image barImage = new Image("media/bar" + score + ".png");
        ImageView barView = new ImageView();
        barView.setImage(barImage);
        barView.setPreserveRatio(true);
        barView.setFitWidth(300);
        this.add(barView, 0, 0);
    }
    
    /**
     * Gets correct background image for current weather conditions,
     * and sets this pane's background accordingly.
     */
    private void setBackground(boolean safe) {
        Image redBG = new Image("media/red-bottom.png");
        Image greenBG = new Image("media/green-bottom.png");
        Image bg = safe? greenBG : redBG;
        this.setBackground(new Background(new BackgroundImage(bg, BackgroundRepeat.NO_REPEAT,
                BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER,
                new BackgroundSize(340, 75, false,
                        false, false, false))));
    }
    
    /**
     * Computes score based on variation from ideal conditions.
     * @param w     WeatherData object.
     * @return      Score from 1 to 5.
     */
    public static int calcScore(WeatherData w) {
        int maxWind=10, minTemp=10, maxTemp=20;
        double currentWind = w.getWindSpeed() * 2.23;
        double score = 1;
        if (w.getTemp() > maxTemp) score += (w.getTemp() - maxTemp) / 5;
        if (w.getTemp() < minTemp) score += (minTemp - w.getTemp()) / 5;
        if (currentWind > maxWind) score += (currentWind - maxWind) / 10;
        if (w.getType() == WeatherData.WeatherType.LIGHT_RAIN) score += 0.5;
        if (w.getType() == WeatherData.WeatherType.HEAVY_RAIN) score += 1.5;
        if (w.getType() == WeatherData.WeatherType.SNOW) score += 2;
        if (score > 5) score = 5;
        return Math.round((float) score);
    }
}
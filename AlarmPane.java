package uk.ac.cam.idesign.group11;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;

import java.time.format.DateTimeFormatter;

//class creates the pane for setting up the alarm time

public class AlarmPane extends GridPane {

    /**
     * Constructor function for the AlarmPane
     * @param clock     Takes an AlarmClock as input, in order to operate on it
     */
    public AlarmPane(AlarmClock clock){
        this.setPadding(new Insets(15, 15, 15, 15));
        this.setMinSize(360, 300);
        this.setVgap(5);
        this.setHgap(5);


        this.add(textEdit(new Text("Rowing Time:")), 0, 0);

        TimeSpinner spinnerRowing = new TimeSpinner();

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("hh:mm:ss a");
        spinnerRowing.valueProperty().addListener((obs, oldTime, newTime) ->
                {
                    System.out.println("New rowing time set by the user" + formatter.format(newTime));
                    clock.setRowAlarm(newTime.getHour(),newTime.getMinute());
                });

        this.add(spinnerRowing, 1, 0);



        this.add(textEdit(new Text("Default time:")), 0, 1);

        TimeSpinner spinnerDefault = new TimeSpinner();

        spinnerDefault.valueProperty().addListener((obs, oldTime, newTime) ->
                {
                    System.out.println("New default time set by the user" + formatter.format(newTime));
                    clock.setDefAlarm(newTime.getHour(),newTime.getMinute());
                });

        this.add(spinnerDefault, 1, 1);

    }


    /**
     * Method for changing the text type
     * @param text
     * @return
     */
    private Text textEdit(Text text){
        text.setFont(Font.font("Roboto", FontWeight.BOLD, 15));
        text.setFill(Color.BLACK);
        text.setTextAlignment(TextAlignment.CENTER);
        return text;
    }



}

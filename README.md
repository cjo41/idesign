# Rowing Weather App

## Introduction

This is a simple weather app developed for the Part IA Interaction Design course at the University of Cambridge.
Authors Cameron O'Connor, Robert Morosanu, Judith Offenberg, Adina Neda and Razvan Nicolescu.
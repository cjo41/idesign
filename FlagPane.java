package uk.ac.cam.idesign.group11;

import javafx.geometry.Pos;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;

/**
 * Pane to show flag. Extends JavaFX's GridPane.
 * @author Group11
 */
public class FlagPane extends GridPane {

    public FlagPane(boolean safe) {
        this.add(createFlag(safe), 0, 0);
        this.setMinWidth(360);
        this.setMaxHeight(105);
        this.setAlignment(Pos.CENTER);
    }
    
    /**
     * Instantiates ImageView for flag image.
     * @param safe      Whether safe or not.
     * @return          Correct flag.
     */
    private ImageView createFlag(boolean safe) {
        Image flag;
        if (safe) flag = new Image("/media/flag-green.png");
        else flag = new Image("/media/flag-yellow.png");
        ImageView view = new ImageView();
        view.setImage(flag);
        view.setFitWidth(340);
        view.setPreserveRatio(true);
        return view;
    }
}

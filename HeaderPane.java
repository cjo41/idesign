package uk.ac.cam.idesign.group11;

import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

/**
 * Pane for the application header. Includes title and alarm icon.
 * @author Group11
 */
public class HeaderPane extends GridPane {

    public HeaderPane() {
        // Configuring pane properties.
        this.setAlignment(Pos.CENTER);
        this.setPadding(new Insets(15, 0, 15, 0));
        ColumnConstraints col1 = new ColumnConstraints();
        col1.setPercentWidth(83);
        ColumnConstraints col2 = new ColumnConstraints();
        col2.setPercentWidth(17);
        this.getColumnConstraints().addAll(col1, col2);

        // Creating title element.
        Text title = new Text("");
        title.setFont(Font.font("Segoe UI", FontWeight.BOLD, 20));
        title.setFill(Color.BLACK);
        this.add(title, 0, 0);

        // Creating alarm icon.
        Image alarm = new Image("media/alarm.png");
        ImageView i = new ImageView();
        i.setImage(alarm);
        i.setFitHeight(45);
        i.setPreserveRatio(true);

        // EventHandler for alarm icon being clicked.
        i.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                System.out.println("Hi!");
            }
        });
        this.add(i, 1, 0);
    }
}
package uk.ac.cam.idesign.group11;

import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;

import java.util.Date;

/**
 * Button for selecting the forecast period to be shown.
 * Extends JavaFX's GridPane.
 * @author Group11
 */
public class TimeButton extends GridPane {
    
    public boolean selected;
    private BottomPane mParent;
    private DataPane mDataPane;
    
    public TimeButton(int timescale, BottomPane parent, DataPane dataPane) {
        // Configuring pane properties.
        mParent = parent;
        this.setAlignment(Pos.CENTER);
        this.setMinHeight(50);
        
        // Defining behaviour when button is clicked.
        this.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                // Updating button highlighting.
                mParent.setAllDeselected();
                selected = true;
                mParent.refreshHighlighting();
                dataPane.update(Main.getForecast(timescale));
            }
        });
        this.add(createText(timescale), 0, 0);
    }
    
    /**
     * Instantiates Text object to show button label.
     * @param timescale     Timescale to show forecast.
     * @return              Timescale label.
     */
    private Text createText(int timescale) {
        String text;
        Date d = new Date();
        int currentHour = d.getHours();
        if (timescale == 0) text = "Now";
        else if (timescale == 1) text = getHourOffset(currentHour, 3);
        else if (timescale == 2) text = getHourOffset(currentHour, 6);
        else text = getHourOffset(currentHour, 9);
        
        Text toDisplay = new Text(text);
        toDisplay.setFont(Font.font("Roboto", FontWeight.BOLD, 15));
        toDisplay.setFill(Color.BLACK);
        toDisplay.setTextAlignment(TextAlignment.CENTER);
        return toDisplay;
    }
    
    /**
     * Takes current hour and offset, and returns the hour at that point
     * in a nice format, for button display.
     * @param current   Current hour.
     * @param offset    Hour offset.
     * @return          The hour at (current + offset), formatted nicely.
     */
    private static String getHourOffset(int current, int offset) {
        int time = (current + offset) % 24;
        if (time < 12) return time + "am";
        else return time + "pm";
    }
}